#!/bin/bash
cd "$(dirname "$0")"

## usage:
##        set.all.sh
## sets all temperatures as defined here

# datapoint to start with
dp=2
# datapoint to send the timestamp/weather
dpWetter=30
# the timestamp (<= 14 bytes)
tstamp=$(date +'%d.%m.%y %H:%M')

cnt=0
./owm_fetch.py |  while read -r line ; do
  let cnt=cnt+1  
  echo "$cnt: $line"
  if [ $cnt -eq 1 ]; then
    #./baos.send.string.js $dpWetter "$line"
    ./baos.send.string.js $dpWetter "$tstamp"
  else
    # today is not interessing (cnt = 2)
    if [ $cnt -eq 2 ]; then
	echo "skipping today..."
    elif [ $cnt -eq 3 -o $cnt -eq 4 ]; then
      ./baos.set.temp.js $dp $(echo "$line" | cut -d' ' -f3) #min
      let dp=dp+1
      ./baos.set.temp.js $dp $(echo "$line" | cut -d' ' -f4) #max
      let dp=dp+1
      ./baos.set.temp.js $dp $(echo "$line" | cut -d' ' -f7) #morn
      let dp=dp+1
      ./baos.set.temp.js $dp $(echo "$line" | cut -d' ' -f8) #eve
      let dp=dp+1
      ./baos.set.temp.js $dp $(echo "$line" | cut -d' ' -f5) #day
      let dp=dp+1
      ./baos.set.temp.js $dp $(echo "$line" | cut -d' ' -f6) #night
      let dp=dp+1
    else
      ./baos.set.temp.js $dp $(echo "$line" | cut -d' ' -f3) #min
      let dp=dp+1
      ./baos.set.temp.js $dp $(echo "$line" | cut -d' ' -f5) #day
      let dp=dp+1
      ./baos.set.temp.js $dp $(echo "$line" | cut -d' ' -f6) #night
      let dp=dp+1
    fi 
  fi
done

