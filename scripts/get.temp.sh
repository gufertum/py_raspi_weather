#!/bin/bash
cd "$(dirname "$0")"

## usage:
##        get.temp.sh <day> min|max|day|night|morn|eve

#shows the current weather (in german as text)
show_weather(){
	fetch_weather | head -1
}

#shows the weather with all temps at the given day from now
show_date(){
	fetch_weather | select_day
}

show_min(){
	fetch_weather | select_day | temp_min
}

show_max(){
	fetch_weather | select_day | temp_max
}

show_day(){
	fetch_weather | select_day | temp_day
}

show_night(){
	fetch_weather | select_day | temp_night
}

show_morn(){
	fetch_weather | select_day | temp_morn
}

show_eve(){
	fetch_weather | select_day | temp_eve
}

fetch_weather(){
 	./owm_fetch.py
}

select_day(){
    #add one more because the script has the 'actual weather in german' as first entry
    let DAY=DAY+2
 	sed -n "$DAY p"
}

temp_min(){
    cut -d' ' -f3
}

temp_max(){
    cut -d' ' -f4
}

temp_day(){
    cut -d' ' -f5
}

temp_night(){
    cut -d' ' -f6
}

temp_morn(){
    cut -d' ' -f7
}

temp_eve(){
    cut -d' ' -f8
}


#select the weather for x days in advance (0 = today)
DAY=0

if [[ "$1" != "" && "$1" =~ ^[0-9]+$ && ("$2" == "min" || "$2" == "max" || "$2" == "day" || "$2" == "night" || "$2" == "morn" || "$2" == "eve") ]]; then
    DAY=$1
    if [[ "$2" == "min" ]]; then
        show_min
    elif [[ "$2" == "max" ]]; then
        show_max
    elif [[ "$2" == "day" ]]; then
        show_day
    elif [[ "$2" == "night" ]]; then
        show_night
    elif [[ "$2" == "morn" ]]; then
        show_morn
    elif [[ "$2" == "eve" ]]; then
        show_eve
    fi
elif [[ "$1" != "" && "$1" =~ ^[0-9]+$ ]]; then
    DAY=$1
    show_date
else
    show_weather
fi
