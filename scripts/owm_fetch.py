#!/usr/bin/env python3
# -*- coding: iso-8859-15 -*-

#import system stuff
import sys
#import pyowm: must be installed first by using 'sudo pip install pyowm'
from pyowm import OWM
#import the config (must contain api_key and api_location)
from owm_fetch_config import *

#check if vars are defined
try:
    API_key
    CFG_loc
except NameError:
    print ("You must defined API_key and CFG_loc in a module called: owm_fetch_config.py!")
    sys.exit(9)

#api key from openweathermap.org and language
owm = OWM(API_key, language='de')
#set the location
observation = owm.weather_at_id(CFG_loc)

#get the current weather
w = observation.get_weather()

#print current weather for testing or displaying
#print(w.get_temperature(unit='celsius'))
#print(w.get_detailed_status() + " (" + str(w.get_temperature(unit='celsius')['temp']) + " grad)")
print(w.get_detailed_status())

# get the forecast (upd. every 3h for 5 days)
#fc = owm.three_hours_forecast_at_id(CFG_loc)

#or

#get the daily forecast for the next x days (max = 16)
# https://openweathermap.org/forecast16
fc = owm.daily_forecast_at_id(CFG_loc, limit=8)

f = fc.get_forecast()
#print len(f)

#iterate over the forcasts and print all the temperatures of each day
lst = f.get_weathers()
for w in f:
    t = w.get_temperature(unit='celsius')
    #print(t)
    print(w.get_reference_time('iso'), t['min'], t['max'], t['day'], t['night'], t['morn'], t['eve'])
