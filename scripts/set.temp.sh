#!/bin/bash
cd "$(dirname "$0")"

## usage:
##        set.temp.sh
## sets all temparatures as defined here

##             <datapoint>         <day> <temp>

## all temps from tomorrow
./baos.set.temp.js 1 $(./get.temp.sh 1 min)
./baos.set.temp.js 2 $(./get.temp.sh 1 max)
./baos.set.temp.js 3 $(./get.temp.sh 1 morn)
./baos.set.temp.js 4 $(./get.temp.sh 1 eve)
./baos.set.temp.js 5 $(./get.temp.sh 1 day)
./baos.set.temp.js 6 $(./get.temp.sh 1 night)

## all temps from the day after tomorrow 
./baos.set.temp.js 7 $(./get.temp.sh 2 min)
./baos.set.temp.js 8 $(./get.temp.sh 2 max)
./baos.set.temp.js 9 $(./get.temp.sh 2 morn)
./baos.set.temp.js 10 $(./get.temp.sh 2 eve)
./baos.set.temp.js 11 $(./get.temp.sh 2 day)
./baos.set.temp.js 12 $(./get.temp.sh 2 night)

