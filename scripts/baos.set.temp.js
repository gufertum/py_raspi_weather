#!/usr/bin/env node

//const dpts = require('knx-dpts-baos');

// usage: baos.set.temp.js <datapointNr> <temperature>
//        send a 4 byte, signed float to the given datapoint

//// print process.argv
//process.argv.forEach(function (val, index, array) {
//  console.log(index + ': ' + val);
//});

if (!(process.argv.length == 4 && !isNaN(process.argv[2]) && !isNaN(process.argv[3]))) {
    console.log("usage: %s <datapointNr> <temperature>", process.argv[1]);
    process.exit(1);
}

//datapoint for the temp (4 byte singed float)
var dp = process.argv[2];

//set the temparature to send to the bus (float, signed, 4 bytes)
var temp = parseFloat(process.argv[3]);

//convert the temp to float buffer
var bufLen = 4; // buffer length used
var dpValue = new Buffer(bufLen)
dpValue.writeFloatBE(temp);

//using library for 2 byte temp (dpt9 is a 2 byte float, so we must use that)
//var dpValue = dpts.dpt9.fromJS(temp); 


console.log("setting datapoint %d to %f", dp, temp);

const Baos = require('../bobaos');
const app = new Baos({serialPort: {device: '/dev/ttyAMA0'}, debug: false});

// switch the light on or off
app.on('open', () => {
  app.setDatapointValue(dp, dpValue).then(_ => {
    console.log('success');
    process.exit(0);
  })
  .catch(err => {
    console.log('error', err);
    process.exit(1);
  });
});

// listen to incoming events and responses
//app.on('service', console.log);
