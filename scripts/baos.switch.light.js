#!/usr/bin/env node

// usage: baos.switch.light.js <datapointNr> ON|OFF
//        switch a light switch using ONE byte: 0 or 1

//// print process.argv
//process.argv.forEach(function (val, index, array) {
//  console.log(index + ': ' + val);
//});

if (!(process.argv.length == 4 && !isNaN(process.argv[2]) && (process.argv[3].toUpperCase() == 'ON' || process.argv[3].toUpperCase() == "OFF"))) {
    console.log("usage: %s <datapointNr> ON|OFF", process.argv[1]);
    process.exit(1);
}

//datapoint for the light switch (1 byte length, use ON an OFF)
var dp = process.argv[2];

var bufLen = 1;
if (process.argv[3].toUpperCase() == 'ON') {
    var dpValue = Buffer.alloc(bufLen, 0xc1) //ON
} else {
    var dpValue = Buffer.alloc(bufLen, 0xc0) //OFF
}

console.log("switching datapoint %d %s", dp, process.argv[3])

const Baos = require('../bobaos');
const app = new Baos({serialPort: {device: '/dev/ttyAMA0'}, debug: false});

// switch the light on or off
app.on('open', () => {
  app.setDatapointValue(dp, dpValue).then(_ => {
    console.log('success');
    process.exit(0);
  })
  .catch(err => {
    console.log('error', err);
    process.exit(1);
  });
});

// listen to incoming events and responses
//app.on('service', console.log);
