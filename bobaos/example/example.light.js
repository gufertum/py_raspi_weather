const Baos = require('../');
const app = new Baos({serialPort: {device: '/dev/ttyAMA0'}, debug: false});

//one byte buffers for light switch
const ON = Buffer.alloc(1, 0xc1)
const OFF = Buffer.alloc(1, 0xc0)

//datapoint for the light switch (1 byte lenght, use ON an OFF)
var dpLight = 1;

// switch the light on or off
app.on('open', () => {
  app.setDatapointValue(dpLight, ON)
});

// listen to incoming events and responses
app.on('service', console.log);

