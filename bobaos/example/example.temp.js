const Baos = require('../');
const app = new Baos({serialPort: {device: '/dev/ttyAMA0'}, debug: false});

//set the temparature to send to the bus (float, signed, 4 bytes)
var temp = -24.5;

//datapoint for the light switch (1 byte lenght, use ON an OFF)
var dpTemp = 2;

//convert the temp to float buffer
var len = 4; // buffer lenght used
var buf = new Buffer()
buf.writeFloatBE(temp)

// switch the light on or off
app.on('open', () => {
  app.setDatapointValue(dpTemp, buf)
});

// listen to incoming events and responses
app.on('service', console.log);

