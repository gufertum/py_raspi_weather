# py_raspi_weather
raspberry pi weather tools using openweathermap

Start by cloning this repo:
```
https://gufertum@bitbucket.org/gufertum/py_raspi_weather.git
```

## TODO

* write glue code (bash) to connect python (get temp) with javascript (send temp)
* cron enty to send temp regularly

## Openweathermap.org

Free API for limited private usage, but an API key must be generated.
The city code for 'Schaan, Liechtenstein' is given below.

* https://openweathermap.org/appid
* http://openweathermap.org/city/3042041

## Final scripts

A shell script is controlling the python and javascript scripts (glue code).

## Javascript

Javascript is used for interacting with the serial port using the bobaos library.

The scripts for sending data to the KNX Bus are located in the **scripts** director.
`<datapoint>` is always the number of the datapoint where the data is expected.

**switch a light on or off**

For this, the KNX datapoint must expect a _1 Byte buffer_, either 0 or 1.

```
./baos.switch.light.js <datapoint> on|off
```

**setting a temperature**

This works by sending a _4 byte buffer_ as _signed float_ to the KNX bus.

```
./baos.set.temp.js <datapoint> <temperature>
```

### Python

The python script is used to retrieve the weather data from openweathermap.org.

**get weather data**

The output of this script:

- **first line**: the actual weather in german
- **7 lines with temperature data**: date time min max day night eve*ning* morn*ing*

```
./owm_fetch.py                                                                                                                                                                                                        0s 
Überwiegend bewölkt
2018-04-25 11:00:00+00 13.08 22.05 22.05 13.08 22.05 18.65
2018-04-26 11:00:00+00 2.92 10.38 9.44 2.92 8.2 9.51
2018-04-27 11:00:00+00 4.98 19.03 17.05 8.67 4.98 16.5
2018-04-28 11:00:00+00 9.34 19.55 18.47 9.34 10.76 17.13
2018-04-29 11:00:00+00 1.86 10.54 10.54 1.86 3.71 6.35
2018-04-30 11:00:00+00 0.81 8.13 8.13 0.81 3.07 4.93
2018-05-01 11:00:00+00 0.53 6.25 6.25 0.53 1.94 4.22
```

### Shell

The shell script is the main interaction point as it fetches the weather data and puts them on the KNX bus.

* **day**: 0 = today, until 7 (next week)
* **temp**: the temperature you want (from witch part of the day)

```
./set.temp.sh <day> min|max|day|night|morn|eve
./set.all.sh
```

set.all.sh is also preferred to run from cron and sets all the temperatures as defined.

## Setup

### Configuration

The  API key must be placed in a separate file, which will be imported by the script.
It does not work without API key.

Create a file called `owm_fetch_config.py` and add the following code:

Or you can also copy the template and add your key there:
```shell
cp owm_fetch_config.py.sample owm_fetch_config.py
#edit the file and add your config data
```

Config Entries needed to run the script:
* API_key: the api key you created
* CFG_loc: the city key

## Language

The script uses python 2.

Python 2 docs: https://docs.python.org/2/library/stdtypes.html#dict

### install dependencies
```
sudo pip install pyowm
#or
sudo pip3 install pyowm
```

### Tooling

Es gibt ein Github Projekt, welche eine einfache Schnittstelle anbietet
Installation einfach mit `pip install pyowm`.
Add your requirements to 'requirements.txt' and run 'pip install -r requirements.txt'.

* https://github.com/csparpa/pyowm

## KNX BAOS Module 383 kBerry

This module is used for connecting the KNX bus to the raspberry pi and consists of a
a Module (GPIO9 and and SDK).

** SDK for the raspberry**

[Download](https://www.weinzierl.de/images/download/software_tools/WzSDK/kdrive/kdriveExpress-16.1.0-raspbian.tar.gz)

### Setup on raspi

Manual setup as seen below. The files can be found in the 'weinzierl' dir and the samples are already adapted (path).

*Quick setup*
```
sudo cp weinzier/lib/raspbian/libkdriveExpress.so /usr/local/lib/kdriveExpress.so
sudo ldconfig

python weinzierl/samples/python/kdrive_express_usb.py
python weinzierl/samples/python/kdrive_express_serial.py
```

*Manual setup*
```
mkdir knx
cd knx
wget https://www.weinzierl.de/images/download/software_tools/WzSDK/kdrive/kdriveExpress-16.1.0-raspbian.tar.gz
tar xvf kdriveExpress-16.1.0-raspbian.tar.gz

cd kdriveExpress-16.1.0-raspbian
sudo cp lib/raspbian/libkdriveExpress.so /usr/local/lib/kdriveExpress.so
sudo ldconfig

#now check out the samples and the path in the python files...
```

### Prepare UART

*see here:* https://github.com/yene/kBerry
*and here:* https://github.com/weinzierl-engineering/baos/blob/master/docs/Raspbian.adoc#kberry

**NEW:** you could probably just **disable 'serial'** in **Preferences** -> **Rasperry Pi Configuration** and skip the rest of this _Prepare UART_ part.

Stick the module on the raspis GPIO pins.
Boot and run this:

```
sudo vim /boot/cmdline.txt
```
Remove this line: 
```
console=serial0,115200
```
Save and reboot.

**WARNING:** Be aware, that everytime raspi-config is executed and the serial shell is enabled, the /boot/cmdline.txt is rewritten with the serial console active again!

Check if the file does not belong to _group_ **tty**. If it still is, then something went wrong.
It should belong to group **dialout**.

```
ls -la /dev/ttyAMA0
```

Now we add our user 'pi' (or the user, which runs the scripts or apps) to that group:

```
sudo usermod -a -G dialout pi
```

Reboot again and UART should work now, which is used for the raspi to communicate with the module.

[Source](https://github.com/weinzierl-engineering/baos/blob/master/docs/Raspbian.adoc#kberry)



### Installation using js library (easier, nearly no depls)

Use this article: https://www.hackster.io/bobalus/bobaos-project-707155

## Requirements

Prepare your Raspberry Pi: install raspbian, enable ssh. Or you could download my image here. In this case you should go directly to step 4. Image is based on 2017-11-29-raspbian-stretch-lite with installed nodejs 8, vim, git, enabled ssh and correct config.txt, cmdline.txt.

* Install KNX BAOS Module 838 kBerry shield
* Set up serial port (-> see 'Prepare UART')
* install git and vim (sudo apt install git vim)

* Install newest node version: https://github.com/audstanley/NodeJs-Raspberry-Pi
```
wget -O - https://raw.githubusercontent.com/audstanley/NodeJs-Raspberry-Pi/master/Install-Node.sh | sudo bash
node -v
```

* Clone this repository
```
git clone https://github.com/shabunin/bobaos.git
```

* Install dependencies
```
cd bobaos/ 
npm install
#add a helper library from: https://github.com/bobaos/knx-dpts-baos/
npm install --save knx-dpts-baos
```
It will install all npm dependencies as serialport.

## Check out the example JS code


Install bdsd.sock and bdsd-cli: https://github.com/bobaos/bobaos

First setup the raspi as before, you you can use the serial port (/dev/ttyAMA0),

Then install the sock: https://github.com/bobaos/bdsd.sock
and run the bdsd.service

```
curl -L https://raw.githubusercontent.com/bobaos/bdsd.sock/master/bdsd_install.sh | bash
```
does not work. manual installation works, but the service cannot start because it cannot connect to the dbus.


Then install the client: https://github.com/bobaos/bdsd-cli

Then use the command line tool to send data:


Alternate method, same software:
```
https://www.hackster.io/bobalus/bobaos-project-707155
```

example script from https://github.com/bobaos/bobaos/blob/master/example/example.js
to turn on or off the light (id  = 1):

* 0xc1 = ON
* 0xc0 = OFF

```
const Baos = require('../');
const app = new Baos({serialPort: {device: '/dev/ttyAMA0'}, debug: false});

// send requests after successful initial reset
app.on('open', () => {
  app.setDatapointValue(1, Buffer.alloc(1, 0xc1))
});

app.on('service', console.log);
```

