#!/bin/sh

#create config file from sample if not already done
if [ ! -f owm_fetch_config.py ]; then
  cp owm_fetch_config.py.sample owm_fetch_config.py
  echo "created owm_fetch_config.py from sample file"
  echo "make sure to update the variabled defined in the file"
fi

# installs python dependencies needed for the script
echo "now make this, but 'sudo'"
echo "pip install pyowm"
echo "     or"
echo "pip3 install pyowm"
